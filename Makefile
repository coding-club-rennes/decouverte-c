##
## EPITECH PROJECT, 2019
## Makefile
## File description:
## Makefile
##

CFLAGS	= -Wall -Wextra

SRC_DIR	= src/

SRC_01	= $(SRC_DIR)ex01.c
SRC_02	= $(SRC_DIR)ex02.c
SRC_03	= $(SRC_DIR)ex03.c
SRC_04	= $(SRC_DIR)ex04.c
SRC_05	= $(SRC_DIR)ex05.c
SRC_06	= $(SRC_DIR)ex06.c

OBJ_01	= $(SRC_01:.c=.o)
OBJ_02	= $(SRC_02:.c=.o)
OBJ_03	= $(SRC_03:.c=.o)
OBJ_04	= $(SRC_04:.c=.o)
OBJ_05	= $(SRC_05:.c=.o)
OBJ_06	= $(SRC_06:.c=.o)

all:	ex01 ex02 ex03 ex04 ex05 ex06

ex01:	$(OBJ_01)
	$(CC) -o ex01 $(OBJ_01)

ex02:	$(OBJ_02)
	$(CC) -o ex02 $(OBJ_02)

ex03:	$(OBJ_03)
	$(CC) -o ex03 $(OBJ_03)

ex04:	$(OBJ_04)
	$(CC) -o ex04 $(OBJ_04)

ex05:	$(OBJ_05)
	$(CC) -o ex05 $(OBJ_05)

ex06:	$(OBJ_06)
	$(CC) -o ex06 $(OBJ_06)

clean:
	$(RM) $(OBJ_01)
	$(RM) $(OBJ_02)
	$(RM) $(OBJ_03)
	$(RM) $(OBJ_04)
	$(RM) $(OBJ_05)
	$(RM) $(OBJ_06)

fclean: clean
	$(RM) ex01 ex02 ex03 ex04 ex05 ex06
