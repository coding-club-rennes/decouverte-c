/*
** EPITECH PROJECT, 2019
** ex02
** File description:
** ex02
*/

#include <stdio.h>

/*
 * Ce programme affiche une chaîne de charactère.
 */

int main()
{
    char *str = "Hello World !";

    printf("%s\n", str);
    return 0;
}
