/*
** EPITECH PROJECT, 2019
** ex01
** File description:
** ex01
*/

#include <unistd.h>

/*
 * my_putchar est une fonction qui prend en paramètre un caractère
 * et l'affiche sur la sortie standard (dans le terminal).
 * Exemple d'affichage d'un 'f' :
 *
 * char c = 'f';
 *
 * my_putchar(c);
 */

void my_putchar(char c)
{
    write(1, &c, 1);
}

int main()
{
    return 0;
}
