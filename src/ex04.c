/*
** EPITECH PROJECT, 2019
** ex04
** File description:
** ex04
*/

#include <stdio.h>

/*
 * Ce programme affiche un tableau d'entier de 6 cases.
 */

int main()
{
    int tab[6] = {4, 6, 2, 7, 11, 0};

    for (int i = 0; i < 6; i++)
        printf("%d\n", tab[i]);
    return 0;
}
